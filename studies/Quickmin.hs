{- porting the DCS solution to Haskell, to do some testing -}
module Quickmin where

data POpts = Empty | AllLt [Int] (Maybe [Int]) | AllGte (Maybe [Int]) [Int] | Split [Int] [Int]

partition :: [Int] -> Int -> Int -> POpts
partition [] _ _ = Empty
partition (x:xs) bound extra =
  case partition xs bound extra of
    Empty ->
      if x < bound then
        AllLt [x] (if extra < bound then Nothing else Just [extra])
      else
        AllGte (if extra < bound then Just [extra] else Nothing) [x]
    AllLt l o ->
      if x < bound then
        AllLt (x:l) o
      else
        case o of
          Nothing -> Split (extra:l) [x]
          Just e -> Split l (x : e)
    AllGte o r ->
      if x < bound then
        case o of
          Nothing -> Split [x] (extra : r)
          Just e -> Split (x:e) r
      else
        AllGte o (x:r)
    Split l r ->
      if x < bound then
        Split (x:l) r
      else
        Split l (x:r)

quickminh :: [Int] -> Int -> Int -> Int
quickminh [] len lower = lower
quickminh (extra:xs) len lower =
  let bound = lower + (len `div` 2) + 1 in
    case partition xs bound extra of
      Empty ->
        if lower < extra then
          lower
        else
          extra + 1
      AllLt l Nothing -> bound
      AllLt l (Just e) -> quickminh e 1 bound
      AllGte Nothing r -> lower
      AllGte (Just e) r ->
        if 1 == bound - lower then
          quickminh r (len - 1) bound
        else
          quickminh e 1 lower
      Split l r ->
        let lenl = length l in
          if lenl == bound - lower then
            quickminh r (len - lenl) bound
          else
            quickminh l lenl lower

quickmin :: [Int] -> Int
quickmin xs = quickminh xs (length xs) 0