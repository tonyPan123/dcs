module SubstTy where

import Syntax
import SyntaxHelpers
import Pos
import ScopeMap
import qualified Data.Map as M
import Constraint

-- map binding positions of variables to types
type TySubst = M.Map Pos Ty

{- substitute type constants of arity 0 whose binding positions (obtained using the ScopeMap)
   are mapped by s.  

   This does not affect meta-variables (see substTyMeta, to be written, for that) -}
substTy :: ScopeMap -> TySubst -> Ty -> Ty
substTy sm s (TAppMeta (m,tys)) = TAppMeta (m,substTy sm s <$> tys)
substTy sm s (Arrow _ arr t1 t2) = Arrow [] arr (substTy sm s t1) (substTy sm s t2)
substTy sm s ty@(TApp (c,[])) =
  case M.lookup (constPos (bindingIf sm c)) s of
    Nothing -> ty
    Just ty' -> ty'
substTy sm s (TApp (c,tys)) = TApp (c,substTy sm s <$> tys)
substTy sm s (TyParens _ ty) = substTy sm s ty

{- compute a substitution mapping the given variables to the given Types associated
   with the given extent. -}
mkTySubst :: [Var] -> [Ty] -> TySubst
mkTySubst vs tys =
  foldr (\ (v,ty) -> M.insert (varPos v) ty) M.empty (zip vs tys)

----------------------------------------------------------------------
-- substitution for meta-variables
--
-- meta-variables are identified by pairs of String and Extent

type MetaTySubst = M.Map MetaVar Ty

mkMetaTySubst :: [MetaVar] -> [Ty] -> MetaTySubst
mkMetaTySubst ms tys =
  foldr (\ (m,ty) -> M.insert m ty) M.empty (zip ms tys)

substTyMeta :: MetaTySubst -> Ty -> Ty
substTyMeta s ty@(TAppMeta (m,tys)) =
  let tys' = substTyMeta s <$> tys in
    case M.lookup m s of
      Nothing -> TAppMeta(m,tys')
      Just ty -> tapp (substTyMeta s ty) tys'
substTyMeta s (Arrow _ arr t1 t2) = Arrow [] arr (substTyMeta s t1) (substTyMeta s t2)
substTyMeta s (TApp (c,tys)) = TApp (c,substTyMeta s <$> tys)
substTyMeta s (TyParens _ ty) = substTyMeta s ty

showMetaTySubst :: MetaTySubst -> String
showMetaTySubst s =
  M.foldrWithKey (\ m ty str -> "\n  " ++ showMetaVarDetails m ++ " -> " ++ showTyDetails ty ++ str) "" s

tyParamTmToMeta :: Extent -> TyParamTm -> Ty
tyParamTmToMeta ext (TyParam v) = varToMetaTy ext v
tyParamTmToMeta ext (TyParamLike _ v _) = varToMetaTy ext v

{- create a new substitution mapping the given type variables to meta-variables based
   on the given extent -}
paramMetaSubst :: Extent -> [Var] -> TySubst
paramMetaSubst ext params = mkTySubst params (varToMetaTy ext <$> params)

paramMetaSubstTm :: Extent -> [TyParamTm] -> TySubst
paramMetaSubstTm ext params = mkTySubst (tyParamTmVar <$> params) (tyParamTmToMeta ext <$> params)

substConstraint :: MetaTySubst -> Constraint -> Constraint
substConstraint s (Subtype ty1 ty2) = Subtype (h ty1) (h ty2)
  where h = substTyMeta s                                     
substConstraint s (Like ty1 ty2) = Like (h ty1) (h ty2)
  where h = substTyMeta s                                     