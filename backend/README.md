# Backend source code

The Haskell source code for the backend is found in this directory.

## Lexer and parser

The lexer and parser for the backend are generated from `Lexer.x` and `Parser.y` using Haskell's standard parser generators, `alex` and `happy`.  Note that comments are stripped out using code in `Comments.hs`, before invoking the lexer.  This means we can write a simple lexer, and add somewhat exotic notation for comments, including comment to beginning of line.  See [examples/comments.dcs](../examples/comments.dcs) for examples.

## Syntax trees and positions

The various datatypes for syntax trees are in `Syntax.hs`, and some helper functions for operating on these are in `SyntaxHelpers.hs`.  Syntax trees store position information, telling where they came from in the input source files.  The type for this is `Pos`, defined in `Pos.hs`.