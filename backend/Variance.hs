module Variance where

{- a 4-point lattice for variances; see helper functions below for the structure -}
data Variance =
    Positive -- type variable occurs at least once, only positively
  | Negative -- type variable occurs at least once, only negatively
  | Mixed    -- type variable occurs at least once positively and at least once negatively
  | Unused  -- type variable does not occur at all
  deriving Eq

instance Show Variance where
  show Positive = "+"
  show Negative = "-"
  show Mixed = "±"
  show Unused = "∅"

invertVariance :: Variance -> Variance
invertVariance Positive = Negative
invertVariance Negative = Positive
invertVariance Mixed = Mixed
invertVariance Unused = Unused

glbVariance :: Variance -> Variance -> Variance
glbVariance Positive Positive = Positive
glbVariance Positive Negative = Mixed
glbVariance Negative Positive = Mixed
glbVariance Negative Negative = Negative
glbVariance Unused x = x
glbVariance x Unused = x
glbVariance _ Mixed = Mixed
glbVariance Mixed _ = Mixed

okFunctorVariance :: Variance -> Bool
okFunctorVariance Positive = True
okFunctorVariance Unused = True
okFunctorVariance _ = False

{- What variance would you be at for occurrence o in type t2[t1],
   if the hole in t2 has variance v2, and o has variance v1 in t?

   Probably there is some algebraic term for this operation... -}
substVariance :: Variance -> Variance -> Variance
substVariance Positive x = x
substVariance Negative x = invertVariance x
substVariance Unused x = Unused
substVariance x Unused = Unused
substVariance Mixed x = Mixed
