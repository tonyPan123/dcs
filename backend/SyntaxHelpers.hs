-- a bunch of utility functions related to the syntax
module SyntaxHelpers where

import Syntax
import Pos
import Highlighting
import Data.List
import Control.Monad


----------------------------------------------------------------------
-- helper functions for syntax
----------------------------------------------------------------------

-- extract all the argument types for the given ctors
typesFromCtrs :: [CtrDef] -> [Ty]
typesFromCtrs = concat . map snd

-- return the list of all Consts c such that TyApp c [] is in tyargs
constsFromTys :: [Ty] -> [Const]
constsFromTys [] = []
constsFromTys ((TApp(c,[])):r) = c:constsFromTys r
constsFromTys (_:r) = constsFromTys r

arrowTy :: [Ty] -> Ty -> Ty
arrowTy tys ty = foldr (Arrow [] ArrowPlain) ty tys

-- if the given Ty is a TApp, return its TApp
unTApp :: Ty -> Maybe TApp
unTApp (TApp ta) = Just ta
unTApp (TyParens _ x) = unTApp x
unTApp _ = Nothing

constFromCtrDef :: CtrDef -> Const
constFromCtrDef = fst

constFromCase :: Case -> Const
constFromCase (Case (Pat c _) _) = c

{- apply the Ty to the args.  If the list of args is nonempty,
   then that Ty should be the kind of
   type that can be applied to args; i.e, anything but an Arrow -}
tapp :: Ty -> [Ty] -> Ty
tapp (TApp (c,args)) args' = TApp $ (c,args ++ args')
tapp (TAppMeta (m,args1)) args2 = TAppMeta (m,args1 ++ args2)
tapp (TyParens _ h) args = tapp h args -- probably won't have parens in here but just in case
tapp ty [] = ty
tapp ty _ = TAppMeta(("TappError",(noPos,noPos)),[])

tyConst :: Const -> Ty
tyConst c = TApp (c,[])

tyMeta :: MetaVar -> Ty
tyMeta m = TAppMeta (m,[])

-- create a new meta-variable for the given variable
varToMeta :: Extent -> Var -> MetaVar
varToMeta ext v = (varStr v, ext)

varToMetaTy :: Extent -> Var -> Ty
varToMetaTy ext v = tyMeta (varToMeta ext v)

tyParamTmVar :: TyParamTm -> Var
tyParamTmVar (TyParam v) = v
tyParamTmVar (TyParamLike _ v _) = v

----------------------------------------------------------------------
-- equality for Ty
----------------------------------------------------------------------
instance Eq Ty where
  (Arrow _ arr ty1 ty2) == (Arrow _ arr' ty1' ty2') = arr == arr' && ty1 == ty1' && ty2 == ty2'
  (TApp(c,tys)) == (TApp(c',tys')) = c == c' && tys == tys'
  (TAppMeta(m,tys)) == (TAppMeta(m',tys')) = m == m' && tys == tys'
  (TyParens _ ty) == ty' = ty == ty'
  ty == (TyParens _ ty') = ty == ty'
  _ == _ = False

----------------------------------------------------------------------
-- show functions for syntax
----------------------------------------------------------------------
data TyFrom = ArrowL | ArrowR | TAppArg 
  deriving (Show, Eq)

permissive = ArrowR 

showTy :: Ty -> String
showTy = showTyLevels False permissive

showTyDetails :: Ty -> String
showTyDetails = showTyLevels True permissive

parensIf :: Bool -> String -> String
parensIf b s = if b then "(" ++ s ++ ")" else s

-- l is the precedence of the immediate context of the given Ty
showTyLevels :: Bool -> TyFrom -> Ty -> String
showTyLevels details from (Arrow _ arr t1 t2) =
  parensIf (from /= ArrowR) (showTyLevels details ArrowL t1 ++ showArrow arr ++ showTyLevels details ArrowR t2 )
showTyLevels details from (TApp u) = showTApp details from u
showTyLevels details from (TAppMeta u) = showTAppMeta details from u
showTyLevels details from (TyParens _ t) = showTyLevels details from t

showTApp :: Bool -> TyFrom -> TApp -> String
showTApp details from (c,[]) =
  if details then
    showVarDetails c
  else
    showVar c
showTApp details from (c,tys) = parensIf (from == TAppArg) (showConst c ++ showListBy (showTyLevels details TAppArg) tys)

showMetaVarDetailsIf :: Bool -> MetaVar -> String
showMetaVarDetailsIf details m =
  if details then
    showMetaVarDetails m
  else
    showMetaVar m

showTAppMeta :: Bool -> TyFrom -> TAppMeta -> String
showTAppMeta details from (m,[]) = showMetaVarDetailsIf details m 
showTAppMeta details from (m,tys) =
  parensIf (from == TAppArg) (showMetaVarDetailsIf details m ++ showListBy (showTyLevels details TAppArg) tys)

showMetaVarDetails :: MetaVar -> String
showMetaVarDetails (v,e) = v ++ "-" ++ showExtent e

showVarDetails :: Var -> String
showVarDetails (v,p) = v ++ "-" ++ show p

showMetaVar :: MetaVar -> String
showMetaVar (v,_) = v

showVar :: Var -> String
showVar (v,_) = v -- fst $ break (== '/') v 
showConst = showVar

showArrow :: Arrow -> String
showArrow ArrowPlain = " → "
showArrow ArrowFat = " ⇒ "

showTm :: Tm -> String
showTm (Var v) = showVar v
showTm (App t1 t2) = "(" ++ showTm t1 ++ " " ++ showTm t2 ++ ")"
showTm (Lam _ v t) = "(λ " ++ showVars v ++ " . " ++ showTm t ++ ")"
showTm (Omega _ f d a b) = "(ω " ++ showVar f ++ "(" ++ showVar d ++ ") : " ++ showTApp False permissive a ++ " . " ++ showTm b ++ ")"
showTm (Gamma _ t cs) = "(γ " ++ showTm t ++ " { " ++ intercalate " | " (showCase <$> cs) ++ "})" 
showTm (Sigma _ bs t) = "(" ++ (join (showSigmaBinding <$> bs)) ++ " . " ++ showTm t ++ ")"
showTm (Parens _ t) = showTm t

showSigmaBinding :: SigmaBinding -> String
showSigmaBinding (SigmaBinding _ x t) = "σ " ++ showVar x ++ " = " ++ showTm t

showCase :: Case -> String
showCase (Case p t) = showPat p ++ " → " ++ showTm t

-- leading blank, blanks between vars
showListBy :: (a -> String) -> [a] -> String
showListBy showA = foldr (\ a r -> " " ++ showA a ++ r) "" 

showVars :: [Var] -> String
showVars = showListBy showVar

showPat :: Pat -> String
showPat (Pat c xs) = showConst c ++ showVars xs

showTyDef :: TyDef -> String
showTyDef (TyDef _ c vs t) = "τ " ++ showConst c ++ showVars vs ++ " = " ++ showTy t

showTmDef :: TmDef -> String
showTmDef (TmDef _ c vs m t) = showConst c ++ showListBy showTyParamTm vs ++ showMaybeTy m ++ " = " ++ showTm t

showTyParamTm :: TyParamTm -> String
showTyParamTm (TyParam v) = showVar v
showTyParamTm (TyParamLike _ v ty) = "(" ++ showVar v ++ " ~ " ++ showTy ty ++ ")"

showMaybeTy :: Maybe Ty -> String
showMaybeTy (Just ty) = " : " ++ showTy ty
showMaybeTy Nothing = ""

showDataDef :: DataDef -> String
showDataDef (DataDef _ c vs cs) =
  "δ " ++ showConst c ++ showVars vs ++ " = " ++
  intercalate " | " (showTApp False permissive <$> cs)

showImport :: Import -> String
showImport (Import _ path _ mq) = "ι " ++ path ++ (case mq of { Nothing -> "" ; Just q -> " ! " ++ showVar q})

showStatement :: Statement -> String
showStatement (TyDefSt s) = show s
showStatement (DataDefSt s) = show s
showStatement (TmDefSt s) = show s

instance Show Pat where
  show = showPat

instance Show Case where
  show = showCase

instance Show Tm where
  show = showTm

instance Show Ty where
  show = showTy

instance Show TyDef where
  show = showTyDef

instance Show DataDef where
  show = showDataDef

instance Show TmDef where
  show = showTmDef

instance Show Statement where
  show = showStatement

--instance Show Import where
--  show = showImport

----------------------------------------------------------------------
-- extracting positions for keywords
----------------------------------------------------------------------
keywordTm :: Haccum Tm 
keywordTm (Var _) hs = hs
keywordTm (Lam pis tp body) hs = punctHighlighting pis $ keywordTm body hs -- handle tp later
keywordTm (App t1 t2) hs = keywordTm t1 (keywordTm t2 hs)
keywordTm (Omega pis _ _ tapp body) hs = punctHighlighting pis $ keywordTApp tapp $ keywordTm body hs
keywordTm (Gamma pis scrut cases) hs = punctHighlighting pis $ keywordTm scrut (foldr keywordCase hs cases)
keywordTm (Sigma pis bs t) hs = punctHighlighting pis $ (foldr keywordSigmaBinding (keywordTm t hs) bs)
keywordTm (Parens pis t) hs = punctHighlighting pis $ keywordTm t hs

keywordSigmaBinding :: Haccum SigmaBinding
keywordSigmaBinding (SigmaBinding pis _ t) hs = punctHighlighting pis (keywordTm t hs)

keywordTy :: Haccum Ty
keywordTy (TAppMeta _) hs = hs
keywordTy (Arrow pis arr t1 t2) hs = punctHighlighting pis $ keywordTy t1 $ keywordTy t2 hs
keywordTy (TApp (v,ts)) hs = foldr keywordTy hs ts
keywordTy (TyParens pis t) hs = punctHighlighting pis $ keywordTy t hs

keywordTApp :: Haccum TApp
keywordTApp (c,tys) hs = foldr keywordTy hs tys

keywordCase :: Case -> [Highlight] -> [Highlight]
keywordCase (Case _ t) hs = keywordTm t hs

keywordFile :: Haccum File
keywordFile (i,b) hs = foldr keywordImport (keywordSimpleFile b hs) i

keywordImport :: Haccum Import
keywordImport (Import pis _ _ _) hs = punctHighlighting pis hs

keywordSimpleFile :: Haccum SimpleFile
keywordSimpleFile [] hs = hs
keywordSimpleFile (s:f) hs = keywordStatement s (keywordSimpleFile f hs)

keywordStatement :: Haccum Statement
keywordStatement (TyDefSt d) hs = keywordTyDef d hs
keywordStatement (DataDefSt d) hs = keywordDataDef d hs 
keywordStatement (TmDefSt d) hs = keywordTmDef d hs

keywordCtrDef :: Haccum CtrDef
keywordCtrDef = keywordTApp

keywordDataDef :: Haccum DataDef
keywordDataDef (DataDef pis c tys ctrs) hs =
  punctHighlighting pis $ foldr keywordCtrDef hs ctrs

keywordTyDef :: Haccum TyDef
keywordTyDef (TyDef pis c tys t) hs =
  punctHighlighting pis $ keywordTy t hs

keywordTmDef :: Haccum TmDef
keywordTmDef (TmDef pis _ params m t) hs =
  punctHighlighting pis $ flip (foldr keywordTyParamsTm) params $ keywordMaybeTy m $ keywordTm t hs

keywordMaybeTy :: Haccum (Maybe Ty)
keywordMaybeTy (Just ty) = keywordTy ty
keywordMaybeTy Nothing = id

keywordTyParamsTm :: Haccum (TyParamTm)
keywordTyParamsTm (TyParam _) hs = hs
keywordTyParamsTm (TyParamLike pis _ ty) hs = punctHighlighting pis $ keywordTy ty hs

----------------------------------------------------------------------
-- functions related to positions in syntax trees
----------------------------------------------------------------------

-- return the starting position of the given statement
posStatement :: Statement -> Pos
posStatement (TyDefSt d) = posTyDef d
posStatement (DataDefSt d) = posDataDef d
posStatement (TmDefSt d) = posTmDef d

posTyDef :: TyDef -> Pos
posTyDef (TyDef (p:_) _ _ _) = p
posTyDef _ = noPos 

posDataDef :: DataDef -> Pos
posDataDef (DataDef (p:_) _ _ _) = p
posDataDef _ = noPos

posTmDef :: TmDef -> Pos
posTmDef (TmDef _ c _ _ _) = constPos c

withinVar :: Pos -> Var -> Bool
withinVar (_,p) (v,(_,p')) = (p' <= p) && (p < p' + length v)
withinConst = withinVar

----------------------------------------------------------------------
-- Extent
--
-- Expressions may be identified by their starting and ending positions.
-- We use these for generating type meta-variables, and reporting
-- arity errors.
----------------------------------------------------------------------

startingPosTm :: Tm -> Pos
startingPosTm (Var v) = varPos v
startingPosTm (Lam (p1:_) _ _) = p1
startingPosTm (App t _) = startingPosTm t
startingPosTm (Omega (p1:_) _ _ _ _) = p1
startingPosTm (Gamma (p1:_) _ _) = p1
startingPosTm (Sigma _ (b:bs) _) = startingPosSigmaBinding b
startingPosTm (Parens (p1:_) _) = p1
startingPosTm _ = noPos

startingPosSigmaBinding :: SigmaBinding -> Pos
startingPosSigmaBinding (SigmaBinding [p1,p2] _ _) = p1
startingPosSigmaBinding _ = noPos
endingPosVar :: Var -> Pos
endingPosVar v = (+ (length (varStr v) - 1)) <$> varPos v 
endingPosConst = endingPosVar

endingPosTm :: Tm -> Pos
endingPosTm (Var v) = endingPosVar v
endingPosTm (Lam _ _ t) = endingPosTm t
endingPosTm (App _ t) = endingPosTm t
endingPosTm (Omega _ _ _ _ t) = endingPosTm t
endingPosTm (Gamma (_:_:p3:_) _ _) = p3
endingPosTm (Sigma _ _ t) = endingPosTm t
endingPosTm (Parens [_,p2] _) = p2
endingPosTm _ = noPos

startingPosTy :: Ty -> Pos
startingPosTy (TAppMeta _) = noPos
startingPosTy (Arrow _ _ t1 _) = startingPosTy t1
startingPosTy (TApp (c,_)) = constPos c
startingPosTy (TyParens (p1:_) _) = p1
startingPosTy (TyParens _ ty) = startingPosTy ty -- should not happen

endingPosTy :: Ty -> Pos
endingPosTy (TAppMeta _) = noPos
endingPosTy (Arrow _ _ _ t2) = endingPosTy t2
endingPosTy (TApp ta) = endingPosTApp ta
endingPosTy (TyParens _ t) = endingPosTy t

endingPosTApp :: TApp -> Pos
endingPosTApp (c,[]) = endingPosConst c
endingPosTApp (c,tys) = endingPosTy (last tys)

extentTyParamTm :: TyParamTm -> Extent
extentTyParamTm (TyParam v) = extentVar v
extentTyParamTm (TyParamLike [p1,_,p2] v _) = (p1,p2)
extentTyParamTm (TyParamLike _ v ty) = (varPos v,endingPosTy ty)

extentStatement :: Statement -> Extent
extentStatement (TyDefSt s) = extentTyDef s
extentStatement (DataDefSt s) = extentDataDef s
extentStatement (TmDefSt s) = extentTmDef s

extentImport (Import (s:_) _ p Nothing) = (s,p)
extentImport (Import (s:_) _ _ (Just v)) = (s,endingPosVar v)
extentImport _ = noExtent

startingPosImport (Import (s:_) _ _ _) = s
startingPosImport (Import _ _ _ _) = noPos

extentTmDef :: TmDef -> Extent
extentTmDef (TmDef _ c _ _ t) = (constPos c, endingPosTm t)

extentDataDef :: DataDef -> Extent
extentDataDef (DataDef (p:p':_) _ _ cs) = (p, maximum (p' : (endingPosTApp <$> cs))) -- p' is for the equals sign
extentDataDef _ = noExtent

extentTyDef :: TyDef -> Extent
extentTyDef (TyDef (p:_) _ _ ty) = (p,endingPosTy ty)
extentTyDef _ = noExtent

extentTm :: Tm -> Extent
extentTm t = (startingPosTm t,endingPosTm t)

extentTy :: Ty -> Extent
extentTy t = (startingPosTy t,endingPosTy t)

extentConst :: Const -> Extent
extentConst c = (constPos c, endingPosConst c)
extentVar = extentConst

startingPosPat :: Pat -> Pos
startingPosPat (Pat c _) = constPos c

extentPat :: Pat -> Extent
extentPat (Pat c []) = extentConst c
extentPat (Pat c vs) = (constPos c, endingPosConst (last vs))

extentCase :: Case -> Extent
extentCase (Case p b) = (startingPosPat p, endingPosTm b)

extentMetaVar :: MetaVar -> Extent
extentMetaVar = snd
----------------------------------------------------------------------

metaVars :: Ty -> [MetaVar]
metaVars = nub . metaVarsh
  where metaVarsh (Arrow _ _ ty1 ty2) = metaVarsh ty1 ++ metaVarsh ty2
        metaVarsh (TApp (c,tys)) = concat $ metaVarsh <$> tys
        metaVarsh (TAppMeta(m,tys)) = m : (concat $ metaVarsh <$> tys)
        metaVarsh (TyParens _ ty) = metaVarsh ty

-- map a metavariable to a constant whose name is derived from the metavariable's
-- name and its extent (for uniqueness).
metaVarToConst :: MetaVar -> Const
metaVarToConst (m,(sp,ep)) = (m ++ "/" ++ show sp ++ "-" ++ show ep,noPos)

----------------------------------------------------------------------
occursIn :: MetaVar -> Ty -> Bool
occursIn m (TAppMeta(m',tys)) = m == m' || (or (occursIn m <$> tys))
occursIn m (TApp(c,tys)) = or (occursIn m <$> tys)
occursIn m (Arrow _ _ ty1 ty2) = occursIn m ty1 || occursIn m ty2
occursIn m (TyParens _ ty) = occursIn m ty

----------------------------------------------------------------------
-- this business about maxConst is broken, where it is used incorrectly
-- in Solver.hs to get a fresh constant.  Solver.hs needs to change to
-- thread a supply (of some kind) of fresh consts.

consts :: Ty -> [Const]
consts (TAppMeta(_,tys)) = join $ consts <$> tys
consts (TApp(c,tys)) = c : (join $ consts <$> tys)
consts (Arrow _ _ ty1 ty2) = consts ty1 ++ consts ty2
consts (TyParens _ ty) = consts ty

maxConst :: Ty -> Int
maxConst ty = maximum $ 0 : ((snd . constPos) <$> consts ty) 

maxConsts :: [Ty] -> Pos
maxConsts tys = mkPos "fresh" (1 + maximum (maxConst <$> tys))

freshConst :: [Ty] -> Ty
freshConst tys = tyConst ("C",maxConsts tys)

----------------------------------------------------------------------

-- variables with a slash cannot be bound, as they are introduced by inference
hasSlash :: String -> Bool
hasSlash = elem '/'

-- keep just the prefix of a string before a slash
dropSlash :: String -> String
dropSlash = fst . break (== '/')
