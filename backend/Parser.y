{

module Parser where

import Pos
import Syntax
import Lexer

import Data.Text(Text,pack,unpack)
import Control.Monad
import System.Environment
import Data.Char

}

%name      parser File
--%name      typee         Type
--%name      terme         Term
--%name      kinde         Kind
--%name      deftermtype   Def
--%name      cmde          Cmd
--%name      liftingtype   LiftingType

%tokentype { Token }
%error     { parseError }
%monad     { Alex }
%lexer     { lexer } { Token _ _ TEOF }

%token
  var        { Token _ _ (Tvar _) }
  'ω'        { Token _ _ Tomega }
  '~'        { Token _ _ Ttilde }
  'γ'        { Token _ _ Tgamma }
  'λ'        { Token _ _ Tlambda }
  ':'        { Token _ _ Tcolon }
  '.'        { Token _ _ Tdot }
  '('        { Token _ _ Tlparen }
  ')'        { Token _ _ Trparen }
  '{'        { Token _ _ Tlbrace }
  '}'        { Token _ _ Trbrace }
  '→'        { Token _ _ Tarrow }
  '⇒'        { Token _ _ Tfatarrow }
  '='        { Token _ _ Teq }
  '|'        { Token _ _ Tpipe }
  'σ'        { Token _ _ Tsigma }
  '!'        { Token _ _ Tbang }
  'δ'     { Token _ _ Tdata }
  'τ'     { Token _ _ Ttype }
  imprt   { Token _ _ (Timport _) }
  nlnl       { Token _ _ Tnlnl }
%%
  
File :: { File }
: OptNlnl ImportsNE nlnl SimpleFileNE { ($2 , $4) }
| OptNlnl SimpleFileNE { ([] , $2) }
| OptNlnl ImportsNE OptNlnl  { ($2 , []) }

ImportsNE :: { Imports }
: Import ImportsNE { $1 : $2 }
| Import { [$1] }

Import :: { Import }
: imprt MaybeQualifier { let str = tStr $1 in
                         let i   = dropWhile isSpace $ tail $ tStr $1 in -- drop the iota and spaces
                         let p1  = tpos $1 in
                         let h ps = Import ps i (fmap (+ length str) p1) in
                           case $2 of
                             Nothing     -> h [p1] Nothing
                             Just (p2,q) -> h [p1,p2] (Just q) }

MaybeQualifier :: { Maybe (Pos,Var) }
: { Nothing }
| '!' Var { Just (tpos $1, $2) }

SimpleFileNE :: { SimpleFile }
: Statement nlnl SimpleFileNE { $1 : $3 }
| Statement OptNlnl { [ $1 ] }

OptNlnl :: { () }
: { () }
| nlnl { () }

Statement :: { Statement }
: TmDef { TmDefSt $1 }
| DataDef { DataDefSt $1 }
| TyDef { TyDefSt $1}

DataDef :: { DataDef }
: 'δ' Var TyParams '=' CtrDefs { let (cs,pis) = $5 in DataDef (tpos $1 : tpos $4 : pis) $2 $3 cs }

TyDef :: { TyDef }
: 'τ' Var TyParams '=' Ty { TyDef [tpos $1, tpos $4] $2 $3 $5 }

Var :: { Var }
: var { (tStr $1, tpos $1) }

TyParams :: { [Var] }
: Var TyParams { $1 : $2 }
| { [] }

TyParamsTm :: { [TyParamTm] }
: Var TyParamsTm { (TyParam $1) : $2 }
| '(' Var '~' Ty ')' TyParamsTm { (TyParamLike [tpos $1 , tpos $3, tpos $5 ] $2 $4) : $6 }
| { [] }

CtrDefs :: { ([CtrDef],[Pos]) } -- the Poss are for the bars between the CtrDefs
: NeCtrDefs { $1 }
| { ([],[]) }

NeCtrDefs :: { ([CtrDef],[Pos]) } 
: CtrDef '|' NeCtrDefs { let (cs,pis) = $3 in ($1 : cs, tpos $2 : pis) }
| CtrDef { ([ $1 ],[]) }

CtrDef :: { CtrDef }
: Var Tys { ($1, $2) }

Tys :: { [Ty] }
: { [ ] }
| NeTys { $1 }

NeTys :: { [Ty] }
: Ty2 { [ $1 ] }
| Ty2 NeTys { $1 : $2 }

-- lowest precedence layer
Ty :: { Ty }
: Ty1 Arrow Ty { let (arr,pi) = $2 in Arrow [pi] arr $1 $3 }
| Ty1 { $1 }

Arrow :: { (Arrow,Pos) }
: '→' { (ArrowPlain,tpos $1) }
| '⇒' { (ArrowFat,tpos $1) }

-- higher precedence layer
Ty1 :: { Ty }
: TAppProper { TApp $1 }
| Ty2 { $1 }

-- highest precedence layer
Ty2 :: { Ty }
: Var { TApp ($1, []) }
| '(' Ty ')' { TyParens (map tpos [$1,$3]) $2 }

TmDef :: { TmDef }
: Var TyParamsTm MaybeTy '=' Tm { let (m,pis) = $3 in TmDef (tpos $4 : pis) $1 $2 m $5 }

MaybeTy :: { (Maybe Ty,[Pos])}
: ':' Ty { (Just $2,[tpos $1]) }
| { (Nothing,[])}

-- lowest precedence layer for terms
Tm :: { Tm }
: 'λ' NeVars1 '.' Tm { Lam (map tpos [$1,$3]) $2 $4 }
| 'ω' Var '(' Var ')' ':' TApp '.' Tm { Omega (map tpos [$1, $3 , $5, $6, $8]) $2 $4 $7 $9 }
| SigmaBindings '.' Tm { Sigma [tpos $2] $1 $3 }
| Tm1 { $1 }

SigmaBindings :: { [SigmaBinding] }
: SigmaBinding { [$1] }
| SigmaBinding SigmaBindings {$1 : $2}

SigmaBinding :: { SigmaBinding }
: 'σ' Var '=' Tm { SigmaBinding [ tpos $1 , tpos $3 ] $2 $4 }

-- next highest precedence layer for terms
Tm1 :: { Tm }
: Tm1 Tm2 { App $1 $2 }
| Tm2 { $1 }

-- highest precedence layer for terms
Tm2 :: { Tm } 
: Var { Var $1 }
| 'γ' Tm '{' Cases '}' { let (cs,pis) = $4 in Gamma (map tpos [$1, $3 , $5] ++ pis) $2 cs }
| '(' Tm ')' { Parens (map tpos [$1,$3]) $2 }

Cases :: { ([Case],[Pos]) }
: { ([],[]) }
| NeCases { $1 }

NeCases :: { ([Case],[Pos]) }
: Case '|' Cases { let (cs,pis) = $3 in ($1 : cs, tpos $2 : pis) }
| Case { ([$1],[]) }

Case :: { Case }
: Pat '→' Tm { Case $1 $3 }

Pat :: { Pat }
: NeVars { let (c,vs) = $1 in Pat c vs }

NeVars :: { (Const,[Var]) }
: Var { ($1,[]) }
| Var NeVars1 { ($1,$2) }

-- same as NeVars but builds different syntax tree
NeVars1 :: { [Var] }
: NeVars { let (c,vs) = $1 in c:vs }

TApp :: { TApp }
: TAppProper { $1 }
| Var { ($1,[]) }
| '(' TApp ')' { $2 } -- we are dropping these parens

TAppProper :: { TApp }
: Var NeTys { ($1,$2) }


{

lexer :: (Token -> Alex a) -> Alex a
lexer f = alexMonadScanErrOffset >>= f
  
parseError :: Token -> Alex a
parseError (Token _ (AlexPn p _ _) t) = alexError ("P" ++ show (p + 1))

type ParseErrorMessage = Either String String

parseTxt :: String -> Text -> Either ParseErrorMessage File
parseTxt filename s =
  let p = (do
             alexSetFilename filename
             parser)
  in 
    case runAlex (unpack s) $ p of
      Prelude.Left  s' -> case head s' of {
                           'L' -> Prelude.Left (Prelude.Left (tail s'));
                           'P' -> Prelude.Left (Prelude.Right (tail s'));
                            _   -> Prelude.Left (Prelude.Right "0") -- This should never happen
                          }
      Prelude.Right r  -> Prelude.Right r

main :: IO ()
main = do  
  [ file ] <- getArgs
  cnt      <- readFile file
  let perr w errMsg = putStrLn (w ++ " error at position " ++ errMsg)
  case parseTxt file (pack cnt) of 
    Prelude.Left  (Prelude.Left  errMsg) -> perr "Lex" errMsg
    Prelude.Left  (Prelude.Right errMsg) -> perr "Parse" errMsg
    Prelude.Right res                    -> putStrLn "Ok" -- >> writeFile ("parser-result.ast") (show res)    

}
