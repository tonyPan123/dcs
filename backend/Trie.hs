{-# LANGUAGE TupleSections #-}
module Trie where

import AssocList

-- tries mapping strings of a's to b's
data Trie a b = Node (Maybe b) (AssocList a (Trie a b))
  deriving Show

trieEmpty :: Trie a b
trieEmpty = Node Nothing assocEmpty

{- this function adds or remove a mapping to a trie, depending on
   whether the Maybe value is Just of something or Nothing. -}
trieUpdate :: Eq a => [a] -> Maybe b -> Trie a b -> Trie a b
trieUpdate [] mb (Node _ ts) = (Node mb ts)
trieUpdate (x:xs) mb (Node mb' ts) =
  -- the trie to update recursively
  let t = case assocLookup x ts of
            Nothing -> trieEmpty
            Just t -> t
  in
    Node mb' (assocUpdate x (trieUpdate xs mb t) ts)

trieInsert :: Eq a => [a] -> b -> Trie a b -> Trie a b
trieInsert xs = trieUpdate xs . Just

trieLookup :: Eq a => [a] -> Trie a b -> Maybe b
trieLookup [] (Node mb _) = mb
trieLookup (x:xs) (Node _ ts) =
  do
    t <- assocLookup x ts 
    trieLookup xs t

{- find the next extension of the given key that is unmapped by the trie,
   where the key gets extended by ext. -}
trieUnique :: Eq a => a -> [a] -> Trie a b -> [a]
trieUnique ext [] t@(Node Nothing _) = []
trieUnique ext [] t@(Node (Just _) ts) = trieUnique ext [ext] t
trieUnique ext i@(x:xs) t@(Node _ ts) =
  case assocLookup x ts of
    Nothing -> i
    Just t ->
      x:(trieUnique ext xs t)


{- return Nothing if there is no match, otherwise Just (pre,val,suff),
   where pre is the longest prefix that matched with the trie, val is
   the value mapped by the trie to pre, and suff is the remaining suffix -}
trieMatchPrefix :: Eq a => [a] -> Trie a b -> Maybe ([a],b,[a])
trieMatchPrefix [] (Node mb _) = ([],,[]) <$> mb
trieMatchPrefix suff@(x:xs) (Node mb ts) =
  {- if we cannot match deeper into ts, we will
     return mb (extended to have the suffix of the input list, and
     whatever prefix) -}
  let notFound pre = (pre,,suff) <$> mb in
    case assocLookup x ts of
      Nothing -> notFound []
      Just t ->
        case trieMatchPrefix xs t of
          Nothing -> notFound []
          Just (pre,val,suff) -> Just (x:pre,val,suff)

trieEx :: Trie Char Int
trieEx = trieInsert "hi" 1 $
         trieInsert "hitch" 2 $
         trieInsert "himself" 3 $
         trieEmpty

fromList :: Eq a => [([a],b)] -> Trie a b
fromList = foldr (uncurry trieInsert) trieEmpty 