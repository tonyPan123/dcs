# Pattern Matching

DCS has, at present, provides a very basic form of pattern matching on inductive data, using a construct `γ`.  In Haskell, this would be `case`, or `match` in OCaml.  For an example,
here is the definition of `or` from [Stdlib/Data/Bool.dcs](../Stdlib/Data/Bool.dcs):

```
or : Bool → Bool → Bool = λ b1 b2 .
  γ b1 {
    True → True
  | False → b2 }
```

This code matches on input `b1` using `γ`. The `γ`-term has clauses that look like `p → t`, where `p` is a pattern.  At present, patterns are rudimentary: just constructors applied to distinct variables.  So nested patterns have to be handled currently by writing nested `γ`-terms.  DCS checks that no constructor is missing, nor any extra constructor present, in the patterns. 

Much fancier forms of pattern matching are available in mature functional languages like Haskell and OCaml.  Perhaps DCS can expand towards this.

In DCS, pattern-matching can be used on any term (called the *scrutinee*) whose type is either a datatype `D` or else an application `D X` of a datatype's signature functor `D`, to some other type `X`.  Under the hood, the patterns are all considered to have the latter form of type (an application of a signature functor), and we constrain the type of the scrutinee to be a subtype of the types of the patterns.  So this means that from a typing perspective, we can pattern-match on anything that is a subtype of a type of the form `D X` where `D` is a signature functor.