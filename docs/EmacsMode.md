# Documentation for the DCS emacs mode

The usual way to interact with DCS is through its emacs mode.  See the top-level README for the project for installing this mode.  When using DCS through emacs, there are actually two modes for interacting with source files:

1. free editing mode, emacs modeline shows `(dcs)`

2. structured editing mode, modeline shows `(dcs!)`

You toggle between modes with `C-c C-l`. Free editing mode just adds some keystrokes for entering Unicode characters. The keystrokes all start with a backslash and then have a single character:

| Keystroke | Unicode character |
| --------- | ----------------- |
| `\o`       | ω |
| `\g`       | γ |
| `\r`       | → |
| `\a`       | ⇒ |
| `\l`       | λ |
| `\d`       | δ |
| `\s`       | σ |
| `\t`       | τ |
| `\i`       | ι |
 
Most of the functionality for DCS is accessed in structured editing mode.  When entering structured editing mode, the file is type-checked and any errors are displayed.


## Keystrokes for structured editing mode

Structured editing mode `(dcs!)` has keystrokes for navigating through the source code, selecting expressions, and
inspecting various computed pieces of information about expressions. 

### Navigation

If your cursor is on a symbol and you enter `.` then you will jump to the introduction of that symbol. If you hit `.` again from that introduction, you will return to the place you started.

Similarly, if your cursor is on an import, you will jump to the imported file if you enter `.`.

### Selection

If your cursor is at the start of a DCS term, then entering `s` will select that term in yellow.  If more than one term begins at that position -- for example because you have an (implicitly left-nested) application like `f a b c` -- then repeatedly hitting `s` will cycle through all those terms.  Similar functionality exists with keystroke `d` if your cursor is at the end of a term.  Keystroke `c` clears the selection.

### Inspection

If your cursor is on a symbol, you can enter `i` to see information about that symbol.  If you have selected a term (using `s` or `d`), then entering `i` will give computed type and subtyping information for that term.

## Errors

If there are errors when processing the file with `C-c C-l`, then a buffer will be shown listing summaries of those errors. Each error will be specially highlighted in the source buffer, with the current error in a box.  If you hit enter on a summary, it will show more detailed error information.  Hitting enter again will hide the details.  With `n` and `p` you can move through the list of errors, and the box showing the current error will move accordingly.  Hit `e` to toggle display of the buffer showing errors.

## Development

You can set the elisp variable `dcs-mode-debug` to print lots of debugging info from the frontend:

```
(setq dcs-mode-debug t)
```

Also, you can use keystroke `z` from structured editing mode to restart the backend.  This is very helpful if you are making changes to the backend.