# Algebras

The central novelty of DCS is in how recursive functions are typed.  In the body of a recursive function, certain values are marked as *recursable* via their types.  Recursable values are given an abstract type `R` specific to the given recursive function, and recursive calls can only be made on values of type `R`.  So recursable values are the only ones that are legal arguments to recursive calls.  DCS provides two special features for working with recursability:

1. A programmer can show, via typing, that a function preserves recursability.  For example, the `tail` function can be written with a type that shows it preserves recursability.  Intuitively, if a list is recursable, then so is its tail.  This sounds trivial, but it is not, because it requires defining `tail` on an input of abstract type `R` (not `List A`). The recursive function can itself preserve recursability, which allows the somewhat exotic feature of nested recursive calls (like `f (f x)` in a recursive definition of `f`).

2. Recursive functions can call constructors to construct data that is recursable in a parent recursion.  So for the example of mergesort (`msort` in [Stdlib/Data/List.dcs](../Stdlib/Data/List.dcs)), the `split` function that is splitting the input list roughly into halves can return these halves at a type that makes them recursable by the parent recursion (the actual `msort` function).

## Syntax of recursive functions

The construct for writing recursive functions is `ω` in DCS.  Algebras are of the form

```
ω f(x) : C . t
```

The parts of this expression are:

- `f`, the name to use in `t` to make recursive calls

- `x`, the input value to the recursion 

- `C`, the carrier of the algebra, which is a type expression that must be provided by the programmer, to help guide type inference.

- `t`, the body of the algebra, which returns that value that should be recursively computed for input `x`

A simple example is `length` from [Stdlib/Data/List.dcs](../Stdlib/Data/List.dcs):

```
  ω length(xs) : K Nat .
    γ xs {
      Nil → Zero
    | Cons x xs → Succ (length xs)
    }
```

Here, the name to use for recursive calls is `length`, and the input list is `xs`.  The carrier of the algebra is `K Nat`.  Let us come back to that shortly.  And the body is the part shown after the first line in the figure (following the `.`). 

The carrier uses the type `K`, defined in [Stdlib/Basic/Bifunctors.dcs](../Stdlib/Basic/Bifunctors.dcs).  `K Nat` is the constant bifunctor that just returns `Nat`.  If one does not need to use DCS's special features for recursability mentioned above (and explained below), then just use `K` with the intended return type of the recursion. Here, the `length` function is returning an element of a different datatype than the input, and so there is no possibility to preserve recursability.  So writing `K Nat` is the right choice.

## Typing of algebras

An algebra that recurses over datatype `D` to produce a value described (in a way to be detailed shortly) by `C` has type `D ⇒ C`.  We call this a `D`-algebra with carrier `C`.

Syntactically, the type `D ⇒ C` intentionally resembles a regular function type like `Nat → Nat`. The difference is in how one `D`-algebra may be invoked inside another `D`-algebra.  In that situation, subtyping can change the type of the `D`-algebra in a special way.  This will be described below.

You might have noticed that while the carrier `C` is included in the syntax of the `ω`-term, the datatype `D` is not. Type inference tries to determine what the datatype is, based on how the input `xs` to the `ω`-term is used.  In particular, as discussed in [PatternMatching.md](PatternMatching.md), `γ`-terms (pattern-matching terms) constrain the type of the scrutinee to be a subtype of the types of the patterns.  And the types of the patterns are computed to be applications of signature functors.  So we can infer `C` for `xs` if it is matched against a pattern.