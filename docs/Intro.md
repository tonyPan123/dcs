# Introduction to DCS

DCS is a functional programming language, whose main goal is to enforce termination of programs written in its core language. Termination is enforced statically, using typing.  The core language provides support for divide-and-conquer programming, using an interface developed in the POPL 2023 "A Type-Based Approach to Divide-and-Conquer Recursion in Coq". 

Several critical commitments of DCS are to:

- subtyping, to find places where coercions should be inserted into programs to make them typable.  DCS accepts the idea of using *nonstructural* subtyping, which means that we can have a subtyping relationship S <: T when S and T do not have the same top-level type constructor

- an interface supporting programmers' navigation of type information.  DCS uses an emacs mode to allow users to navigate through the complex type information computed by the DCS backend, in order to understand code and reported errors.

- monads, to allow writing programs which do not fit into the core language.  As of writing, this work has not been undertaken yet, but will include a monad for possibly diverging programs, as well as a monad for impure programs (like Haskell's IO monad)

This document describes DCS's approach at a high level.  More detailed information can be found in other files in the [docs](../docs) directory.

## General background on enforcing termination

DCS enforces that every program written in its core language is guaranteed to terminate on all inputs, without any exceptions (finite failure) or divergence (infinite failure).  Enforcing termination is a rather exotic property for a programming language to enforce.  Indeed, of currently available implemented languages, only type theories like Coq, Agda, and Lean enforce termination (Haskell, OCaml, Racket, etc. do not).  They do so by a compile-time *syntactic* check for *structural* termination.  The check is syntactic in the sense that it examines the source code of a program.  It is structural, because it essentially is just requiring that recursive functions are only called on subdata of their inputs.  Examples of subdata are tails of lists, subtrees of trees, and predecessors of natural numbers. Structural termination is, unfortunately, a brittle property: refactoring code can cause the termination checker to reject a function it previously accepted. The problem is discussed more in [Structural.md](Structural.md).

DCS uses a different approach to termination, based on typing.  To understand this, it is helpful to consider some small examples, to see what the termination checker needs to rule out. Here is a very artificial Haskell program, which recurses on an input natural number and produces Zero.

```
data Nat = Zero | Succ Nat

toZero :: Nat -> Nat
toZero Zero = Zero
toZero (Succ x) = toZero x
```

This function is structurally terminating.  In the second clause, it recurses on `x`, which is a structural subdatum of the input `Succ x`.  Viewing inductive datatypes as describing trees, the `x` tree is a subtree of `Succ x`.

### Non-terminating variants

It would obviously be unsound, from the point of view of termination, to accept this alternative:

```
toZero :: Nat -> Nat
toZero Zero = Zero
toZero (Succ x) = toZero (Succ x)
```

Here, in the second clause, we recurse on the very same value we started with.  The code will then loop on any nonzero input.  So we cannot allow recursive calls on the same value we started with. 

Here is another problematic variant:

```
toZero :: Nat -> Nat
toZero Zero = toZero (Succ Zero)
toZero (Succ x) = toZero x
```

The second clause has a structurally decreasing recursive call now, but in the first clause, we are calling `toZero` on a value bigger than the input value: in a case for `Zero`, we recurse on `Succ Zero`.  This code will also loop, naturally, as any will eventually end up bouncing back and forth between `toZero Zero` and `toZero (Succ Zero)`.

So the termination checker cannot allow recursive calls on bigger values.

Here's another problematic case:

```
toZero :: Nat -> Nat -> Nat
toZero Zero _ = Zero
toZero (Succ x) y = toZero y y
```

What if we call `toZero (Succ Zero) (Succ Zero)`?  Then the `x` of the second clause is `Zero`, and the `y` is `Succ Zero`.  But that means the recursive call `toZero y y` is again the call we started with, namely `toZero (Succ Zero) (Succ Zero)`.  So this function will loop in that case. 

So the termination checker cannot allow recursive calls on values unrelated to the *parameter of recursion* (the argument that is supposed to be decreasing as we recurse).

### Terminating variants that are not structurally terminating

Here is a variant that is actually just fine, but will be rejected by the termination checkers of Agda, Coq, and Lean.  Suppose we have the following function for dividing a `Nat` by 2:

```
half Zero = Zero
half (Succ Zero) = Zero
half (Succ (Succ x)) = Succ (half x)
```

Then the following function is indeed terminating, but will not pass a structural termination checker:

```
toZeroHalf :: Nat -> Nat
toZeroHalf Zero = Zero
toZeroHalf (Succ x) = toZeroHalf (half x)
```

This is not structurally terminating, because in the second clause, we recurse on `half x`, which is not syntactically a piece of the input.  On the other hand, it is guaranteed to be no bigger than `x`, and so it must be strictly smaller than the input value `Succ x`. (Amazingly, Agda actually accepts this definition, and so must be checking something beyond just structural termination.)

Here is another example. Suppose we have the following higher-order function:

```
iter :: (Nat -> Nat) -> Nat -> Nat -> Nat
iter f a Zero = a
iter f a (Succ x) = f (iter f x)
```

This takes in a function `f` and naturals `a` and `x`, and returns `f (f ... (f a))` where there are `x` calls to f.  This function is structurally terminating, because where `iter` is called recursively, it is with `x`, which is a subdatum of the input `Succ x`.

But consider now this variant of `toZero`, which calls `iter`:

```
toZeroIterId :: Nat -> Nat
toZeroIterId Zero = Zero
toZeroIterId (Succ x) = toZeroIterId (iter id x x)
```

This function is not structurally terminating, again because the recursive call (in the second clause) is not literally on `x`, but rather `iter id x x`.  That expression evaluates to `x`, but no termination checker I am aware of is going to be able to deduce that. 

In Coq, Agda, and Lean, while `toZeroIterId` is not accepted, one could write a variant of it using *well-founded recursion*.  With that technique, one specifies a well-founded ordering on the type of the parameter of recursion, and then must prove that the arguments to recursive calls are smaller than the input to the function, in this ordering.  The proofs are actually part of the code, and so the resulting function is not literally `toZeroIterId`, but ignoring the proof arguments, it is the same. 

DCS's approach can be viewed as an alternative to well-founded recursion.  As with well-founded recursion, the programmer is supplying additional information, beyond just the syntactic definition of the function. But in DCS, programmers show how their functions can be written using a specific typed interface for recursion.  So instead of termination proofs, in DCS one must solve programming problems: how can I write my function using DCS's interface for recursion?  This raises quite different problems, ones which decidedly have a programming character.

## DCS's approach to enforcing termination 

As mentioned, DCS enforces termination not by restricting the syntactic form of programs, but via typing.  The starting point is so-called Mendler-style recursion.  Let us introduce this for the specific case of recursion over lists (other datatypes are similar).  Let us write `List A` for the type of lists of values of type `A`.  Suppose we want to define the `length` function on such lists.  With Mendler-style recursion, we do so given the following:

- a type variable `R`; since it is a variable, our code for `length` cannot assume anything about this type

- the list on which we are recursing, except that the tail of the list has type `R` instead of `List A`

- a function, let us call it `lengthR`, of type `R -> Nat`

Then our code needs to compute the length.  If the list on which we are recursing is `Nil`, then we return `0` as expected.  Otherwise, the list is of the form `Cons x xs`, where `x : A` is the head of the list, and `xs : R` could be thought of as the abstracted tail of the list.  The beauty of Mendler's approach is that now we can make any calls we want to `lengthR`, as long as they are type correct.  There is actually only one such call, actually, which is `lengthR xs`.  This call is type correct, because `lengthR` expects an input of type `R`, and `xs` indeed has type `R`.  But we can pass `lengthR` to another function, wrap `xs` in obfuscating calls, etc., and typing will ensure that no matter what is happening in the term, the only way we can call `lengthR` is with `xs`. 

In DCS, we would write `length` in Mendler style like this (from [Stdlib/Data/List.dcs](../Stdlib/Data/List.dcs)):

```
ω length(xs) : K Nat .
  γ xs {
    Nil → Zero
  | Cons x xs → Succ (length xs)
  }
```

The `ω` introduces a DCS *algebra*, which is what is used to write recursive functions.  Critically, as explained above, in the body of this algebra (the part after the dot that occurs at the end of the first line), `length` has type `R → Nat`, where `R` is an abstract type introduced in the body of the algebra.  And `xs : R`, so the call type checks.

The next steps to understand DCS are to learn more about

- [Datatypes.md](Datatypes.md). Datatypes and their signature functors are declared simultaneously in DCS.

- [PatternMatching.md](PatternMatching.md). Pattern-matching in DCS is shallow and has to cover all constructors for the datatype.

- [Algebras.md](Algebras.md). Recursive functions in DCS are written as algebras.  These guarantee termination, while supporting divide-and-conquer programming, where it may be necessary to build a new, but smaller, data structure and recurse on it.  DCS uses a novel type-based approach to ensure this is done in a way that preserves termination.  Algebras have their own special type `F ⇒ C` in DCS.