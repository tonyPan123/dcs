# Structural termination

Structural termination covers a surprisingly broad range of useful programs on natural numbers, lists, etc.  But sadly, it has a number of serious problems.

1. It is sensitive to code refactoring, in the sense that code which passed the termination checker before refactoring may fail to do so afterwards.  In particular, abstracting out subexpressions from the body of a recursive function may break termination checking.

2. It does not directly handle divide-and-conquer recursion, where an input value is split into smaller values, which are then recursively processed, and the results are then recombined.  The splitting step can produce values that are smaller but are not syntactic subexpressions on the starting value.