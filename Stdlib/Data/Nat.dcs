ι ../Basic/Bifunctors
ι Bool

δ Nat = Zero | Succ Nat

pred (R ~ Nat) : R → R = λ x . γ x { Zero → x | Succ p → p }

add : Nat → Nat → Nat =
  ω add(x) : K (Nat → Nat) .
    γ x {
      Zero → λ y . y
    | Succ x → λ y . Succ (add x y)
    }

sub (R ~ Nat) : R → Nat → R =
  λ x . ω sub(y) : K R .
          γ y {
            Zero → x
          | Succ y' → pred (sub y')
          }

mul : Nat → Nat → Nat =
  ω mul(x) : K (Nat → Nat) .
    γ x {
      Zero → λ y . Zero
    | Succ x' → λ y . add y (mul x' y)
    }

τ ZeroR R P = P

zeroR : Nat ⇒ ZeroR =
  ω _(xs) : ZeroR .
    γ xs {
      Zero → Zero
    | Succ _ → Zero
    }

lt =
  ω lt(n) : K (Nat → Bool) .
    λ m .
    γ n {
      Zero → γ m {
               Zero → False
             | Succ _ → True
             }
    | Succ n → γ m {
                 Zero → False
               | Succ m → lt n m
               }
    }

τ H R P = P

div (R ~ Nat) : R → Nat → R =
  λ x y .
  (ω div(x) : H .
    γ x {
      Zero → x
    | Succ x' →
      γ lt x y {
        True → zeroR x
      | False → Succ (div (sub x' (pred y)))
      }
    }
  ) x

eqnat =
  ω eqnat(n) : K (Nat → Bool) .
    λ m .
    γ n {
      Zero → γ m {
               Zero → True
             | Succ _ → False
             }
    | Succ n → γ m {
                 Zero → False
               | Succ m → eqnat n m
               }
    }

pow : Nat → Nat → Nat = λ b .
  ω powb(e) : K Nat .
    γ e {
      Zero → Succ Zero
    | Succ e' → mul b (powb e')
    }

