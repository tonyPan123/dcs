;; Register errors from the backend and handle displaying them nicely  -*- lexical-binding: t; -*-
;;
;; Errors have:
;;   - a brief message, which should contain no newlines
;;   - a start and end position
;;   - a detailed message which could have newlines.
;;     It should end in a newline.  It can be nil if there are
;;     no details for this error)

(require 'dcs-keymaps)
(require 'dcs-debug)

(defvar-local dcs-mode-errors nil
  "List of errors we received from the backend the previous time we loaded the file (excluding parse errors and root errors)")

(defun dcs-mode-parsing-error(phase pos)
  "Print an error message about a parsing problem"
  (message (concat phase " error"))
  (goto-char pos))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; unanchored errors are currently just for issues with roots

(defun dcs-mode-unanchored-error(msg)
  "Print an unanchored error message in a buffer"
  (let ((b (dcs-mode-unanchored-error-buffer)))
    (display-buffer b)
    (select-window (get-buffer-window b))
    (goto-char (point-max))
    (insert msg)))

(defvar dcs-mode-unanchored-error-buffer-name "*dcs-unanchored-errors*"
  "Return the name for the inspect buffer for the current dcs editing buffer")

(defun dcs-mode-unanchored-error-buffer ()
  "Return the inspect buffer for the current dcs editing buffer"
  (get-buffer-create dcs-mode-unanchored-error-buffer-name))

(defun dcs-mode-clear-unanchored-errors()
  "Clear the buffer showing unanchored errors"
  (let ((b (dcs-mode-unanchored-error-buffer)))
    (when b
      (with-current-buffer b (erase-buffer)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; different types of errors that the backend may send us

; once errors are all recorded, we will sort the list of errors 
; (see dcs-mode-create-error-buffer below).
(defun dcs-mode-record-error(briefmsg startpos endpos details)
  "Given an error, record it in `dcs-mode-errors'."
  (push (list briefmsg (cons startpos endpos) details)
        dcs-mode-errors))

(defun dcs-mode-undefined-sym(startpos endpos var)
  "Highlight an undefined symbol at the given location."
  (dcs-mode-record-error (concat var " undefined") startpos endpos nil)
  (dcs-mode-highlight-error startpos endpos))

(defun dcs-mode-variance-error(startpos endpos details)
  "Highlight an illegal use of a datatype in its own definition, at the given extent."
  (dcs-mode-record-error "Datatype variance issue" startpos endpos details)
  (dcs-mode-highlight-error startpos endpos))

(defun dcs-mode-arity-error(startpos endpos msg)
  "Highlight a kinding error at the given location."
  (dcs-mode-record-error "Arity error for type expression" startpos endpos msg)
  (dcs-mode-highlight-error startpos endpos))

(defun dcs-mode-type-error(startpos endpos msg)
  "Highlight a typing error at the given location."
  (dcs-mode-record-error "Type error" startpos endpos msg)
  (dcs-mode-highlight-error startpos endpos))

(defun dcs-mode-import-error(startpos endpos msg)
  "Highlight an error related to importing files, at the given location."
  (dcs-mode-record-error "Import error" startpos endpos msg)
  (dcs-mode-highlight-error startpos endpos))

(defun dcs-mode-root-error(msg)
  "Highlight an error related to importing files, at the given location."
  (dcs-mode-unanchored-error msg))

(defvar-local dcs-mode-error-buffer-parent nil
  "The dcs editing buffer from which this error buffer was created.")

(defun dcs-mode-clear-errors()
  "Clear any errors received from the backend the previous time we loaded the file."
  (setq dcs-mode-errors nil))

; this is for when we need to re-highlight the source
(defun dcs-mode-highlight-errors()
  "Highlight all errors currently recorded"
  (when dcs-mode-errors
    (dolist (e dcs-mode-errors)
      (let* ((ext (nth 1 e))
             (startpos (car ext))
             (endpos (cdr ext)))
        (dcs-mode-highlight-error startpos endpos)))))

(defun dcs-mode-error-buffer-name ()
  "Return the name for the error buffer for the current dcs editing buffer"
  (concat "*dcs-error-" (dcs-mode-current-buffer-base-name) "*"))

(defun dcs-mode-error-buffer ()
  "Return the error buffer for the current source buffer"
  (let ((b (get-buffer-create (dcs-mode-error-buffer-name)))
        (cur (current-buffer)))
    (with-current-buffer b
      (setq dcs-mode-error-buffer-parent cur)       ; set parent buffer, needed in jump-to-error below
      (use-local-map dcs-mode-error-buffer-map))    ; set the keymap
    b))

(defun dcs-mode-error-buffer-window-if()
  "Return the error buffer window if it is present"
  (let ((b (if dcs-mode-error-buffer-parent
               (current-buffer) ; if a buffer has an error-buffer-parent,
                                ; then it is an error buffer!
               (dcs-mode-error-buffer))))
    
    (get-buffer-window b)))

(defun dcs-mode-select-error-buffer-window()
  "Display the error buffer window associated with this dcs buffer"
  (interactive)
  (let* ((b (dcs-mode-error-buffer))
         (w (get-buffer-window b)))
    (unless w (display-buffer b)) ; create a window for the buffer if there isn't one
    (dcs-mode-highlight-errors) ; rehighlight errors
    (select-window (get-buffer-window b)))) ; select that window

(defun dcs-mode-compare-errors(e1 e2)
  (let ((s1 (car (nth 1 e1)))
        (s2 (car (nth 1 e2))))
    (< s1 s2)))

(defun dcs-mode-toggle-error-buffer-window()
  "Toggle display of the error buffer"
  (interactive)
  (let ((w (dcs-mode-error-buffer-window-if)))
    (if w
      (delete-window w)
      (dcs-mode-select-error-buffer-window))))

(defun dcs-mode-error-buffer-quit-window()
  "Quit the window showing the error buffer"
  (interactive)
  (quit-window)
  (let ((w (get-buffer-window dcs-mode-error-buffer-parent)))
    (select-window w)
  ))

; hook to add to window-selection-change-functions, to watch for when
; the error buffer is selected.  When it is, we hide the cursor in the
; source window
(defun dcs-mode-error-buffer-selection-change(win)
    "Toggle the cursor in the source window depending on whether the error buffer window is selected or not"
    (let ((ew (dcs-mode-error-buffer-window-if)))
      (when (eq ew win)
       (let* ((w (get-buffer-window dcs-mode-error-buffer-parent))
              (s (selected-window))
              (in-error-buffer (eq s win)))
         (when w ; the source window is present
         (select-window w) ; select the source window

         ; change cursor type depending on whether or not we entered
         ; this function with the error buffer selected
         (setq cursor-type (if in-error-buffer nil 'box))

         (select-window s))))))

(defun dcs-mode-on-quit-error-buffer()
  "When the error buffer is quitted, reset the source buffer's cursor type."
  (let ((w (get-buffer-window dcs-mode-error-buffer-parent))
        (c (selected-window)))
      (when w ; the source window is present
        (select-window w) 
            
        ; change cursor type depending on whether or not we entered
        ; this function with the error buffer selected
        (setq cursor-type 'box)

        (select-window c))))


(defun dcs-mode-create-error-buffer()
  "If there were errors the last time we loaded the file, set up the error buffer with these"
  (setq dcs-mode-errors (sort dcs-mode-errors 'dcs-mode-compare-errors))
  (let ((es dcs-mode-errors))

    (dcs-mode-select-error-buffer-window)
  
    (add-hook 'quit-window-hook
              'dcs-mode-on-quit-error-buffer 0 t)

    (add-hook 'window-selection-change-functions
              'dcs-mode-error-buffer-selection-change 0 t)

    (setq buffer-read-only nil
          window-size-fixed nil)

    (erase-buffer)

    (when es
        (progn
          ; now insert the text for the errors
          (dolist (e es)
            (let ((p1 (point)))
              (insert (nth 0 e))
              (insert "\n")

              ; remember the details for the error
              (let ((p2 (point)))
                (put-text-property p1 p2 'dcs-mode-error-extent (nth 1 e))
                (put-text-property p1 p2 'dcs-mode-error-details (nth 2 e))
                )))
  ;        (fit-window-to-buffer)
          (setq buffer-read-only t)
          (goto-char 1)
          (dcs-mode-error-buffer-jump-to-error)
          ))

      ; quit the error window
      (quit-window)

      ))

(defun dcs-mode-line-extent()
  "Return the extent (start and end position) for the current line; do not change point"
  (let ((p (point)))
    (beginning-of-line)
    (let ((p1 (point)))
      (forward-line)
      (let ((p2 (point)))
        (goto-char p)
        (cons p1 p2)))))
  
(defun dcs-mode-error-buffer-toggle-details()
  "Toggle display of the error details (if any) for the error at point"
  (interactive)
  (let* ((ext (get-text-property (point) 'dcs-mode-error-details-extent))
         (det (get-text-property (point) 'dcs-mode-error-details))
         (lext (dcs-mode-line-extent))
         (sl (car lext))
         (el (cdr lext)))

    ; we store the extent of the error details shown as a pair of offsets from the 
    ; end of the brief message line

    (setq buffer-read-only nil)
    (if ext
      ; hide the error details
      (progn
        (delete-region (+ el (car ext)) (+ el (cdr ext)))
        (put-text-property sl el 'dcs-mode-error-details-extent nil))

      ; show the error details
      (if det
        (progn
          (goto-char el)
          (insert det) ; should be non-nil at this point

          ; remember the extent of the details message we just inserted
          (put-text-property sl el 'dcs-mode-error-details-extent (cons 0 (- (point) el))))
        (message "No error details")))
    (goto-char sl)
    (setq buffer-read-only t)))

          
(defun dcs-mode-error-buffer-jump-to-error()
  "Jump to the location of the error at point in the error buffer"
  (interactive)
  (let* ((cur (current-buffer))
         (w (get-buffer-window dcs-mode-error-buffer-parent))
         (extent (get-text-property (point) 'dcs-mode-error-extent)))
    (if w
        (progn
          (select-window w)
          ; you have to select the window to move the point in that
          ; window.  Otherwise, you are moving the point in the buffer only
          ; (according to the docs, buffers have N+1 points, one for the buffer
          ; itself and then N for the N windows displaying it.
          (when dcs-mode-errors
              (when extent
                (let* ((startpos (car extent))
                       (endpos (cdr extent)))
                  (dcs-mode-debug-message
                   "Jumping in %s to %d"
                   (buffer-name dcs-mode-error-buffer-parent)
                   startpos)
                  (goto-char startpos)
                  (dcs-mode-set-error-focus startpos endpos))))
          (select-window (get-buffer-window cur)))
      (message "%s has been closed"
               (buffer-name dcs-mode-error-buffer-parent)))))

(defun dcs-mode-error-buffer-next-error()
  "Jump to the next error from the error buffer"
  (interactive)

  ; advance a line, wrapping around
  (when (or (eq (forward-line 1) 1) ; forward-line 1 will return 1 if it did not advance 1 line
            (eq (point) (point-max))) ; we are at the end of the error buffer, but that has no error there
    (goto-char 1))

  (dcs-mode-error-buffer-jump-to-error))

(defun dcs-mode-error-buffer-previous-error()
  "Jump to the previous error from the error buffer"
  (interactive)

  ; go back a line, wrapping around
  (when (eq (forward-line -1) -1) ; forward-line 1 will return 1 if it did not advance 1 line
    (goto-char (point-max))
    (forward-line -1)) ; then go back one line, because no error on last line of error buffer

  (dcs-mode-error-buffer-jump-to-error))
    

(provide 'dcs-errors)
