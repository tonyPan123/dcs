;; -*- lexical-binding: t; -*-

(defgroup dcs ()
  "Customization group for `dcs-mode'."
  :group 'languages)

(defcustom dcs-mode-backend
  (let* ((self (if (fboundp 'macroexp-file-name)
                   (macroexp-file-name)
                 load-file-name))
         (selfdir (if self (file-name-directory self))))
    (when selfdir
      (expand-file-name "../backend/dist/build/dcs/dcs" selfdir)))
  "The executable to run for the dcs backend, with which the Emacs mode communicates"
  :type '(file))

(defcustom dcs-mode-debug nil
  "If non-nil then print debug messages for dcs mode"
  :type '(boolean))

(defcustom dcs-mode-comment-color "sienna"
  "The color for comments"
  :type '(color))

(defcustom dcs-mode-keyword-color "lime green"
  "The color for keywords"
  :type '(color))

(defcustom dcs-mode-error-color "orange"
  "The color for errors"
  :type '(color))

(defcustom dcs-mode-navigation-color "yellow"
  "The color for highlighting subterms during navigation"
  :type '(color))

(provide 'dcs-customization)
