open import lib

module SemTree where

data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

data TreeE : Set where
  Leaf : TreeE
  Node : TreeE → TreeE → TreeE

TreeT : ∀(A : Set) → TreeE → Set
TreeT A Leaf = ⊥
TreeT A (Node l r) = TreeF A (TreeT A l) (TreeT A r)

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 L1 R2 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- R input needed, to avoid illegally injecting Leaf into R      
     (up1 : R1 → R)
     (up2 : R2 → R)     
     (up3 : L1 → R1)
     (up4 : L2 → R2)     
     (sfo1 : FoldT Alg A R1 L1)
     (sfo2 : FoldT Alg A R2 L2)      
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

size : TreeE → ℕ
size Leaf = 0
size (Node l r) = suc (size l + size r)

foldh : (n : ℕ)(e : TreeE) → size e ≡ (suc n) → {A : Set} → FoldT (SAlg A) A (TreeT A e) 
foldh 0 (Node l r) p {A} func (mkSAlg alg) x = alg (SAlg A) {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} {!!} ? ? ? ? ? ?
foldh (suc n) e p {A} func (mkSAlg alg) x = {!!} 


{-
data Tree(A : Set) : ℕ → Set where
  Leaf : ∀{n : ℕ} → Tree A (suc n)
  Node : ∀{n1 n2 : ℕ} → A → Tree A n1 → Tree A n2 → Tree A (suc (n1 + n2))

removeLeft : ∀{n : ℕ}{A : Set} → Tree A (suc n) → maybe (A × Tree A n)
removeLeft Leaf = nothing
removeLeft (Node x l r) with l
removeLeft (Node x l r) | Node x1 l1 r1 with removeLeft l
removeLeft (Node x l r) | _ | just (y , l') = just (y , Node x l' r)
removeLeft (Node x l r) | _ | nothing = just (x , {!!}) 
removeLeft (Node x l r) | Leaf = just (x , {!!})
-}