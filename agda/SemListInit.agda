{- working out the new interface for SAlg, using the init function
   as a concrete example -}

open import lib
open import Iter

module SemListInit where

data ListF(A X : Set) : Set where
  Nil : ListF A X
  Cons  : A → X → ListF A X

IListF : Set → ℕ → Set
IListF A = IterT (ListF A) ⊥

List : Set → Set
List A = Σ ℕ (IListF A)

In : ∀{A : Set} → ListF A (List A) → List A
In Nil = (1 , Nil)
In (Cons a (n , v)) = (suc n , Cons a v)

Out : ∀{A : Set} → List A → ListF A (List A)
Out (zero , v) = Nil
Out (suc n , Nil) = Nil
Out (suc n , Cons a v) = Cons a (n , v)

nil : ∀{A : Set} → List A
nil = In Nil

cons : ∀{A : Set} → A → List A → List A
cons a xs = In (Cons a xs)

inita : ∀(n : ℕ){A : Set} → ListF A (IListF A n) → maybe (IListF A n)
inita n Nil = nothing
inita (suc n) (Cons a xs) with inita n xs
inita (suc n) (Cons a xs) | nothing = just xs
inita (suc n) (Cons a xs) | just xs' = just (Cons a xs')

initd : ∀{L R A : Set} → (R → (ListF A L -> R) × maybe L) -> ListF A R → maybe R
initd _ Nil = nothing
initd rec (Cons a xs) with rec xs
initd rec (Cons a xs) | (_ , nothing) = just xs
initd rec (Cons a xs) | (up , just xs') = just (up (Cons a xs'))

id : ∀{X : Set} → X → X
id x = x

inith : ∀(n : ℕ){A : Set} → ListF A (IListF A n) → maybe (IListF A n)
inith zero l = initd{⊥}{⊥} (λ()) l
inith (suc n) {A} l = initd{IListF A n}{IListF A (suc n)} (λ x → ( id , inith n x)) l

initr : ∀{A : Set} → List A → maybe (List A)
initr (suc n , v) with inith n v
initr (suc n , v) | nothing = nothing
initr (suc n , v) | just r = just (n , r)

