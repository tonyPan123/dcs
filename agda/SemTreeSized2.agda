open import lib

module SemTreeSized2 where

-- index is for nesting depth of functor applications
data Tree(A : Set) : ℕ → Set where
  Leaf : Tree A 1
  Node : ∀{n m : ℕ} → A → Tree A (suc n) → Tree A (suc m) → Tree A (suc (suc (n + m)))

-- 'loose'
Treel : Set → ℕ → Set
Treel A n = Σ ℕ (λ m → m ≤ n ≡ tt ∧ Tree A m)

data TreeF(A R1 R2 : Set) : Set where
  Leaf : TreeF A R1 R2
  Node : A → R1 → R2 → TreeF A R1 R2

Tzero : ∀{A : Set} → Treel A zero → ∀{X : Set} → X
Tzero (zero , p , ()) 
Tzero (suc m , () , t) 

Tone : ∀{A : Set}→ Treel A 1 → TreeF A ⊥ ⊥
Tone (suc .0 , p , Leaf) = Leaf
Tone (suc (suc .(n' + m')) , () , Node{n'}{m'} x t t₁) 

T≤ : ∀{A : Set}{m n : ℕ} → m ≤ n ≡ tt → Treel A m → Treel A n
T≤{A}{m}{n} p2 (l , p1 , t) = (l , ≤-trans{l}{m}{n} p1 p2 , t)

Tsuc : ∀{A : Set}{n : ℕ} → Treel A n → Treel A (suc n)
Tsuc{A}{n} t = T≤ (≤-suc n) t

Tinversion' : ∀{A : Set}{n : ℕ} → Treel A n → Σ ℕ (λ l → n ≡ suc l)
Tinversion' {A} {suc n} (.1 , p , Leaf) = (n , refl)
Tinversion' {A} {suc n} (.(suc (suc (n1 + n2))) , p , Node {n1} {n2} x t t₁) = (n , refl)

Tinversion : ∀{A : Set}{n : ℕ} → Tree A n → Σ ℕ (λ l → n ≡ suc l)
Tinversion {A} {.1} Leaf = (0 , refl)
Tinversion {A} {.(suc (suc (n1 + n2)))} (Node{n1}{n2} x t t₁) = (suc (n1 + n2) , refl)

TabstIn1 : ∀{A : Set}{n1 n2 n : ℕ} → n1 + n2 ≤ n ≡ tt → 
           TreeF A (Treel A n1) (Treel A (suc n2)) → Treel A (suc n)
TabstIn1 {A} {n1} {n2} {n} _ Leaf = (1 , ≤0 n , Leaf)
TabstIn1 {A} {n1} {n2} {n} p (Node x (n1' , p1' , l) (n2' , p2' , r)) with Tinversion l | Tinversion r
TabstIn1 {A} {n1} {n2} {n} p (Node x (n1' , p1' , l) (n2' , p2' , r)) | (n1'' , q1) | (n2'' , q2) rewrite q1 | q2 =
  (suc (suc (n1'' + n2'')) ,
   (≤-trans{suc n1'' + n2''}{n1 + n2}{n}
     (≤-trans{suc n1'' + n2''}{n1 + n2''}{n1 + n2} (≤+mono1{suc n1''}{n1}{n2''} p1') (≤+mono2{n1}{n2''}{n2} p2'))
     p) ,
   Node x l r)

TabstIn2 : ∀{A : Set}{n1 n2 n : ℕ} → n1 + n2 ≤ n ≡ tt → 
           TreeF A (Treel A (suc n1)) (Treel A n2) → Treel A (suc n)
TabstIn2 {A} {n1} {n2} {n} _ Leaf = (1 , ≤0 n , Leaf)
TabstIn2 {A} {n1} {n2} {n} p  (Node x (n1' , p1' , l) (n2' , p2' , r)) with Tinversion l | Tinversion r
TabstIn2 {A} {n1} {n2} {n} p (Node x (n1' , p1' , l) (n2' , p2' , r)) | (n1'' , q1) | (n2'' , q2) rewrite q1 | q2 =
  (suc (suc (n1'' + n2'')) ,
   q ,
   Node x l r)
  where q : suc (n1'' + n2'') ≤ n ≡ tt
        q rewrite sym (+suc n1'' n2'') =
          (≤-trans{n1'' + suc n2''}{n1 + n2}{n}
            (≤-trans{n1'' + suc n2''}{n1 + suc n2''}{n1 + n2}
              (≤+mono1{n1''}{n1}{suc n2''} p1') (≤+mono2{n1}{suc n2''}{n2} p2'))
            p)

id : ∀{X : Set} → X → X
id x = x

-- two arguments corresponding to the arity (2) of TreeF
Carrier : Set₁
Carrier = Set → Set

Func : Carrier → Set₁
Func C = ∀{A A' : Set} → (A → A') → C A → C A'

SAlgK : Set₁
SAlgK = Carrier → Set

FoldT : SAlgK → Set → Set → Set → Set₁
FoldT Alg A R L = ∀{X : Carrier} → Func X → Alg X → R → X L

SAlgBody : SAlgK → Carrier → Set → Set₁
SAlgBody Alg1 X A =
  (∀ (Alg : SAlgK)(R R1 R2 L1 L2 : Set)
     (abstIn1 : R → TreeF A L1 R2 → R) -- R input needed, to avoid illegally injecting Leaf into R
     (abstIn2 : R → TreeF A R1 L2 → R) -- similarly
     (up1 : L1 → R1)
     (up2 : L2 → R2)     
     (up3 : R1 → R)
     (up4 : R2 → R)     
     (sfo1 : FoldT Alg A R1 L1)
     (sfo2 : FoldT Alg A R2 L2)
     (embedAlg : ∀{Y : Carrier} → Alg1 Y → Alg Y)
     (alg : Alg X)
     (d : TreeF A R1 R2) → 
     X R)

{-# NO_POSITIVITY_CHECK #-}
{-# NO_UNIVERSE_CHECK #-}
data SAlg(A : Set) : SAlgK where
  mkSAlg : ∀ {X : Carrier} →
           SAlgBody (SAlg A) X A → 
           SAlg A X

foldh : (n : ℕ) → ↓𝔹 _>_ n → {A : Set} → FoldT (SAlg A) A (Treel A (suc n)) (Treel A n)
foldh zero _ {A} func (mkSAlg alg) t =
  alg (SAlg A) (Treel A zero) ⊥ ⊥ ⊥ ⊥
    (λ t → Tzero t) (λ t → Tzero t) (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) (Tone t)
foldh (suc n) _ {A} func (mkSAlg alg) (m , p , Leaf) =
  alg (SAlg A) (Treel A (suc n)) ⊥ ⊥ ⊥ ⊥
    (λ _ _ → (1 , ≤0 n , Leaf)) (λ _ _ → (1 , ≤0 n , Leaf))
    (λ())(λ())(λ())(λ()) (λ _ _ ()) (λ _ _ ()) id (mkSAlg alg) Leaf
foldh (suc n) (pf↓ w) {A} func (mkSAlg alg) (m , p , Node{n1}{n2} a l r) =
  let n1≤ = (≤-trans{n1}{n1 + n2}{n} (≤+1 n1 n2) p) in
  let n2≤ = (≤-trans{n2}{n1 + n2}{n} (≤+2 n1 n2) p) in
    alg (SAlg A) (Treel A (suc n)) (Treel A (suc n1)) (Treel A (suc n2)) (Treel A n1) (Treel A n2)
      (λ _ → TabstIn1{A}{n1}{n2}{n} p) (λ _ → TabstIn2{A}{n1}{n2}{n} p) Tsuc Tsuc (T≤ n1≤) (T≤ n2≤)
      (foldh n1 (w (≤<-trans{n1}{n}{suc n} n1≤ (<-suc n))))
      (foldh n2 (w (≤<-trans{n2}{n}{suc n} n2≤ (<-suc n))))
      id (mkSAlg alg) (Node a (suc n1 , ≤-refl (suc n1) , l) (suc n2 , ≤-refl (suc n2) , r))

RemoveLeft : Set → Carrier
RemoveLeft A R = maybe (A × R)

RemoveLeftFunc : ∀ {A : Set} → Func (RemoveLeft A)
RemoveLeftFunc = {!!}

removeLeft : ∀{A : Set} → SAlg A (RemoveLeft A)
removeLeft{A} = mkSAlg body
  where body : SAlgBody (SAlg A) (RemoveLeft A) A
        body Alg R R1 R2 L1 L2 abstIn1 abstIn2 up1 up2 up3 up4 sfo1 sfo2 embedAlg alg Leaf = nothing
        body Alg R R1 R2 L1 L2 abstIn1 abstIn2
             up1 up2 up3 up4 sfo1 sfo2 _ alg (Node x l r) with sfo1 RemoveLeftFunc alg l 
        body Alg R R1 R2 L1 L2 abstIn1 abstIn2
             up1 up2 up3 up4 sfo1 sfo2 _ alg (Node x l r) | nothing = just (x , up4 r)
        body Alg R R1 R2 L1 L2 abstIn1 abstIn2
             up1 up2 up3 up4 sfo1 sfo2 _ alg (Node x l r) | just (a , l') = just (a , abstIn1 (up4 r) (Node x l' r))
{-
∀{n : ℕ}{A : Set} → Tree A (suc n) → maybe (A × Tree A n)
removeLeft Leaf = nothing
removeLeft (Node x l r) with l
removeLeft (Node x l r) | Node x1 l1 r1 with removeLeft l
removeLeft (Node x l r) | _ | just (y , l') = just (y , Node x l' r)
removeLeft (Node x l r) | _ | nothing = just (x , {!!}) 
removeLeft (Node x l r) | Leaf = just (x , {!!})
-}