Require Import Kinds.
Require Import Functors.

Section Mu.

  Variable F : Set -> Set.
  Context {FunF : Functor F}.           

  Definition MAlgebra
             (A : Set) :=
    forall (R : Set), (R -> A) -> F R -> A.

  Inductive Mu : Set := 
    mu : MAlgebra Mu.

  Definition inMu(d : F Mu) : Mu :=
    mu Mu (fun x => x) d.

  Definition outMu(m : Mu) : F Mu :=
    match m with
    | mu A r d => fmap r d
    end.

  Definition retraction : forall(d : F Mu), outMu (inMu d) = d.
  simpl.
  intro d.
  rewrite fmapId.
  trivial.
  Qed.
  
End Mu.

Section MuAlg.

  Variable F : KAlg -> KAlg.
  Variable algMap : forall {A B : KAlg}, CastAlg A B -> CastAlg (F A) (F B). 


  Inductive MuAlg : KAlg := 
  muAlg : forall A : KAlg,
    (forall (X : Carrier), A X -> MuAlg X) ->
    forall (X : Carrier), F A X -> MuAlg X.

  Definition inMuAlg {X : Carrier} (d : (F MuAlg) X) : MuAlg X :=
    muAlg MuAlg (fun X x => x) X d.

  
  Definition outMuAlg{X : Carrier} (v : MuAlg X) : (F MuAlg) X :=
    match v with
    | muAlg A r X1 d => algMap r X1 d
    end.
End MuAlg.
