# Contributors

The following people have contributed to DCS, either through work on the DCS tool or adding DCS code to this repo.  The list grows by addition at the end, and contributions may be summarized by gitlab MR id. 

- Aaron Stump, main implementor, project lead

- Stefan Monnier, language design, emacs improvements

- Robert Zhang, MR 2, MR 4

- Shiwei Weng, foldr

- Sage Binder, MR 5

- [@DeSevilla1](https://gitlab.com/DeSevilla1), MR 14